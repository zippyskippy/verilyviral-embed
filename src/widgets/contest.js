import React from 'react';
import ReactDOM from 'react-dom';


function Title(props) {
    return "VerilyViral Title " + props.vid;
}

const container = document.querySelector('[vvid]');
const vid = document.querySelector('[vvid]')?.attributes?.vvid?.value;
const title = document.querySelector('.verilyviral-title');
const video = document.querySelector('.verilyviral-video');
const description = document.querySelector('.verilyviral-description');
const join = document.querySelector('.verilyviral-join');

if (!vid) return;

ReactDOM.render(Title({ vid: vid || 'missing vvid' }), container);

