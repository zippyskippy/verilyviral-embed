
const path = require('path');
const webpack = require('webpack');
// const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: ['./src/widgets/contest.js',],
    output: {
        // move bundle.js to a folder instead the root
        // path: path.join(__dirname, 'public'),
        filename: 'contest.js'
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    // module: {
    //     rules: [
    //         {
    //             test: /\.js$/,
    //             include: path.resolve('./widgets/contest.js'),
    //             loader: 'babel-loader',
    //             exclude: /node_modules/,
    //             query: require('./.babelrc'),
    //         }
    //     ]
    // },
    //   plugins: [
    //     new webpack.optimize.UglifyJsPlugin(),
    //     // New plugin
    //     new HtmlWebpackPlugin({
    //       // injects bundle.js to our new index.html
    //       inject: true,
    //       // copys the content of the existing index.html to the new /build index.html
    //       template:  path.resolve('./index.html'),
    //     }),
    //  ]
}